package Testdemo4;
import static org.junit.Assert.assertEquals;

import org.junit.*;
public class Test4 {

		static Armstrong obj;
		@BeforeClass
		public static void  ba() {
			obj = new Armstrong();
			System.out.println("I am BeforeClass");
		}
		
		@Before
		public void be() {
			System.out.println("I am Before");
		}
		
		@Test
		public void t1() {
			assertEquals(obj.arm(10),false);
			System.out.println("I am Testcase 1");
		}
		
		@Test
		public void t2() {
			assertEquals(obj.arm(153),true);
			System.out.println("I am Testcase 2");
		}
		
		@AfterClass
		public static void aa() {
			System.out.println("I am AfterClass");
		}
		
		@After
		public void ae() {
			System.out.println("I am After");
		}
}


